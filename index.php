<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Repetition Control Structures and Array Manipulation</title>
    </head>
    <body>

        <h1>Array Manipulation</h1>
        <p><?php iterate(); ?></p>


        <h1>Array Manipulation</h1>
        <?php array_push($students, 'John Smith') ?>
        <pre>Array 1: <?php print_r($students) ?></pre>
        <pre><?php echo count($students) ?></pre>

        <?php array_push($students, 'Jane Smith') ?>
        <pre>Array 2: <?php print_r($students) ?></pre>
        <pre><?php echo count($students) ?></pre>

        <?php array_shift($students) ?>
        <pre>Array 3: <?php print_r($students) ?></pre>
        <pre><?php echo count($students) ?></pre>


    </body>
</html>